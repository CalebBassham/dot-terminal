ZSH_PLUGINS=$HOME/.terminal/zsh/plugins
HISTFILE=$HOME/.zsh_history
SAVEHIST=1000
setopt inc_append_history
setopt share_history

alias gaa="git add -A"
alias gc="git commit"

source $ZSH_PLUGINS/oh-my-zsh/lib/history.zsh
source $ZSH_PLUGINS/oh-my-zsh/lib/completion.zsh
source $ZSH_PLUGINS/zsh-autosuggestions/zsh-autosuggestions.zsh
source $ZSH_PLUGINS/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

source $HOME/.terminal/zsh/prompt.sh
