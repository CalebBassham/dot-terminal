#! /usr/bin/env bash

cd $HOME/.terminal
git pull
git submodule update --init --recursive
source $HOME/.terminal/zsh/zshrc.sh
